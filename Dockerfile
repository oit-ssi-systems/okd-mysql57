FROM stevedore-repo.oit.duke.edu/el-base-el7
LABEL maintainer='Drew Stinnett <drew.stinnett@duke.edu>'

LABEL vendor='Duke University, Office of Information Technology, Automation'
LABEL architecture='x86_64'
LABEL summary='OIT Systems MySQL 5.7 container'
LABEL description='CentOS-based MySQL 5.7 container for use with web applications'
LABEL distribution-scope='private'
LABEL authoritative-source-url='https://stevedore-repo.oit.duke.edu'

LABEL version='1.0'
LABEL release='1'

ENV TERM xterm
ENV CUSTOM_CONF_DIRS "/conf /var/www/html/.stevedore"
ENV DB_TYPE mysql

RUN rpm -Uvh https://repo.mysql.com/mysql57-community-release-el7.rpm
RUN yum install -y pwgen hostname mysql-server python36-mysql python36-PyYAML \
    && yum clean all

ADD utf8.cnf /etc/my.cnf.d/utf8.cnf

RUN mkdir -p /etc/service/mysqld
ADD mysqld.run /etc/service/mysqld/run
RUN chmod -R a+x /etc/service/mysqld

ADD healthcheck.sh /healthcheck.sh
ADD check_or_init.py /check_or_init.py
RUN chmod 755 /check_or_init.py
ADD tests.d/* /tests.d/

EXPOSE 3306
