#!/bin/bash
set -eo pipefail

cred_dir='/conf/.creds'

host="$(hostname --ip-address || echo '127.0.0.1')"
user="root"

if [[ -f ${cred_dir}/dbdata.yaml ]] ; then
  dbname="$(awk '/name/ {print $2}' ${cred_dir}/dbdata.yaml)"
  dbpass="$(awk '/mysql/ {print $2}' ${cred_dir}/dbdata.yaml)"
else
  echo >&2 "No ${cred_dir}/dbdata.yaml - exiting"
  exit 1
fi

args=(
        # force mysql to not use the local "mysqld.sock" (test "external" connectibility)
        -h"$host"
        -u"$user"
        -p"$dbpass"
        --silent
)

if select="$(echo 'SELECT 1' | mysql "${args[@]}")" && [ "$select" = '1' ]; then
        exit 0
fi

exit 1

