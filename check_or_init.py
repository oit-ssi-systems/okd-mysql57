#!/usr/bin/env python3
import sys
import yaml
import os
import MySQLdb


def get_temp_password():
    """
    MySQL generates a random root password if one is not set, and dumps it in
    a log
    """
    log_lines = os.popen(
        "cat /var/log/mysqld.log  | grep 'A temporary password is generated for root@localhost'"
    ).readlines()

    try:
        password = log_lines[-1].split(' ')[-1].strip()
    except:
        password = None
    return password


def generate_credential_file(cred_file):
    """
    Generate a credential file to use.  The end format is:

    ---
    name: $name_of_database
    mysql: $mysql_root_pw
    backup: $mysql_back_pw
    """

    parent_directory = os.path.dirname(cred_file)
    if not os.path.exists(parent_directory):
        os.makedirs(parent_directory, exist_ok=True)

    data = dict(
        name=os.popen("pwgen -1 16").readline().strip(),
        mysql=os.popen("pwgen -c -n -y -1 16").readline().strip(),
        backup=os.popen("pwgen -c -n -y -1 16").readline().strip()
    )

    with open(cred_file, 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=False, explicit_start=True)

    print("Created credential file: %s" % cred_file)


def parse_credential_file(cred_file):
    """
    Given a credential file, return an object representing that data
    """

    with open(cred_file, 'r') as stream:
        data = yaml.load(stream)

    return {
        'database_name': data['name'],
        'root_password': data['mysql'],
        'backup_password': data['backup']
    }


def test_mysql_login(username, password, host='localhost'):
    """
    Test to see if a username/password works
    """
    db = MySQLdb.connect(host=host, user=username, passwd=password)
    db.close()
    return True


def create_mysql_init_file(mysql_data, file_path='/mysql-init.txt'):
    """
    Create an initialization file that the mysql process will always run on
    startup
    """
    f = open(file_path, 'w')
    f.write("DELETE FROM mysql.user;\n")
    f.write("ALTER USER 'root'@'localhost' IDENTIFIED BY \"%s\"; \n"
            % mysql_data['root_password'])
    f.write(
        "CREATE DATABASE IF NOT EXISTS %s;\n" % mysql_data['database_name'])
    f.write(
        "GRANT ALL PRIVILEGES on *.* to 'backup'@'%%' IDENTIFIED BY '%s';\n"
        % mysql_data['backup_password'])
    f.write(
        "GRANT ALL PRIVILEGES on *.* to 'root'@'%%' IDENTIFIED BY '%s';\n"
        % mysql_data['root_password'])
    f.write(
        "GRANT ALL PRIVILEGES on *.* to 'root'@'localhost' IDENTIFIED BY '%s';\n"
        % mysql_data['root_password'])
#   f.write(
#       "UPDATE mysql.user SET Password=PASSWORD(\"%s\") WHERE User='root';\n"
#       % mysql_data['root_password'])
    f.write("FLUSH PRIVILEGES;\n")
    f.close()

    return True


def main():
    cred_file = '/conf/.creds/dbdata.yaml'

    #print(get_temp_password())

    # Create a credential file if needed
    if not os.path.exists(cred_file):
        generate_credential_file(cred_file)

    db_info = parse_credential_file(cred_file)

    create_mysql_init_file(db_info)

    return 0


if __name__ == "__main__":
    sys.exit(main())
